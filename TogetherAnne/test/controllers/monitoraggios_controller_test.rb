require 'test_helper'

class MonitoraggiosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @monitoraggio = monitoraggios(:one)
  end

  test "should get index" do
    get monitoraggios_url
    assert_response :success
  end

  test "should get new" do
    get new_monitoraggio_url
    assert_response :success
  end

  test "should create monitoraggio" do
    assert_difference('Monitoraggio.count') do
      post monitoraggios_url, params: { monitoraggio: { calcolo: @monitoraggio.calcolo, data: @monitoraggio.data, linguaggio: @monitoraggio.linguaggio, memoria: @monitoraggio.memoria, orientamentosp: @monitoraggio.orientamentosp, richiamo: @monitoraggio.richiamo, totale: @monitoraggio.totale } }
    end

    assert_redirected_to monitoraggio_url(Monitoraggio.last)
  end

  test "should show monitoraggio" do
    get monitoraggio_url(@monitoraggio)
    assert_response :success
  end

  test "should get edit" do
    get edit_monitoraggio_url(@monitoraggio)
    assert_response :success
  end

  test "should update monitoraggio" do
    patch monitoraggio_url(@monitoraggio), params: { monitoraggio: { calcolo: @monitoraggio.calcolo, data: @monitoraggio.data, linguaggio: @monitoraggio.linguaggio, memoria: @monitoraggio.memoria, orientamentosp: @monitoraggio.orientamentosp, richiamo: @monitoraggio.richiamo, totale: @monitoraggio.totale } }
    assert_redirected_to monitoraggio_url(@monitoraggio)
  end

  test "should destroy monitoraggio" do
    assert_difference('Monitoraggio.count', -1) do
      delete monitoraggio_url(@monitoraggio)
    end

    assert_redirected_to monitoraggios_url
  end
end
