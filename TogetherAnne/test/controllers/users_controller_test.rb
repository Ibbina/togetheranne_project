require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
test "should get new" do
    get :new
    assert_response :success
  end


  test "should redirect edit when logged in as wrong user" do
    log_in_as(@user)
    get :edit, id: @user
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@user)
    patch :update, id: @user, user: { nome: @user.nome, cognome: @user.cognome,
                                      email: @user.email, citta: @user.citta,
                                      regione: @user.regione, date: @user.date, 
				      demenza: @user.demenza}

    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect destroy when not logged in" do
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@user)
    
      delete :destroy, id: @user
    assert_redirected_to root_url
  end


end
