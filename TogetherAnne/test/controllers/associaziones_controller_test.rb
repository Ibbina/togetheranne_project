require 'test_helper'

class AssociazionesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @associazione = associaziones(:one)
  end

  test "should get index" do
    get associaziones_url
    assert_response :success
  end

  test "should get new" do
    get new_associazione_url
    assert_response :success
  end

  test "should create associazione" do
    assert_difference('Associazione.count') do
      post associaziones_url, params: { associazione: { codicea: @associazione.codicea, cognomea: @associazione.cognomea, nomea: @associazione.nomea } }
    end

    assert_redirected_to associazione_url(Associazione.last)
  end

  test "should show associazione" do
    get associazione_url(@associazione)
    assert_response :success
  end

  test "should get edit" do
    get edit_associazione_url(@associazione)
    assert_response :success
  end

  test "should update associazione" do
    patch associazione_url(@associazione), params: { associazione: { codicea: @associazione.codicea, cognomea: @associazione.cognomea, nomea: @associazione.nomea } }
    assert_redirected_to associazione_url(@associazione)
  end

  test "should destroy associazione" do
    assert_difference('Associazione.count', -1) do
      delete associazione_url(@associazione)
    end

    assert_redirected_to associaziones_url
  end
end
