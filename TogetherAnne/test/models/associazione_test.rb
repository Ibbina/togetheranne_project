require 'test_helper'

class AssociazioneTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end

  test "should be valid" do
    assert @associazione.valid?
  end

  test "codicea should be present" do
    @associazione.codicea = nil
    assert_not @associazione.valid?
  end

  test "nomea should be present " do
    @associazione.nomea = "   "
    assert_not @associazione.valid?
  end
		
  test "cognomea should be present " do
    @associazione.cognomea = "   "
    assert_not @associazione.valid?
  end


end
