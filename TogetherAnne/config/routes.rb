Rails.application.routes.draw do

  devise_for :users, :controllers => { :omniauth_callbacks => "omniauth_callbacks" }
  #devise_scope :user do
    #get 'users/sign_in', :to => 'devise/sessions#new', :as => :new_user_session
  #end
  
	
  #devise_for :users, :controllers => { :callbacks_controller_controller => "controllers/callbacks_controller_controller" }

  root 'static_pages#home'

  get 'static_pages/suggerimenti'

  get 'static_pages/aboutus'
  
  resources :users do
  	resources :monitoraggios
	resources :associaziones
	#get 'users/sign_in', :to => 'devise/sessions#new', :as => :new_user_session
	#delete 'users/sign_out', :to => 'devise/sessions#destroy', :as => :destroy_session
  end
  

  #devise_scope :user do
    #get ' /users/sign_in', :to => 'devise/sessions#new', :as => :new_user_session
    #get 'sign_out', :to => 'users/sessions#destroy', :as => :destroy_user_session
  #end
 
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
