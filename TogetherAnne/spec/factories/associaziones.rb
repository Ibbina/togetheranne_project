FactoryBot.define do
require 'faker'

  factory :associazione do 
    user_id     { Faker::IDNumber.valid }
    nomea       {Faker::Name.first_name}
    cognomea    {Faker::Name.last_name}
    codicea     {Faker::IDNumber.valid}
  end
end
