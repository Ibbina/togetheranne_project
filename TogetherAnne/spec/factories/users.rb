FactoryBot.define do
require 'faker'
  factory :user do
    nome           {Faker::Name.first_name}
    cognome        {Faker::Name.last_name}
    email          {Faker::Internet.safe_email}
    password       {"password"}
    date           {Faker::Date.birthday(min_age:13,max_age:99)}
    citta          {Faker::Address.city}
    demenza        {true}
    luogonascita   {"Roma"}
    regione        {"Lazio"}
    indirizzo      {"Via bho 1"}
    nomeass        {Faker::Name.first_name}
    cognomeass     {Faker::Name.first_name}
  end

end

