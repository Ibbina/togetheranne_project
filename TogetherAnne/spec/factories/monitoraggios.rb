FactoryBot.define do
require 'faker'

  factory :monitoraggio do 
    user_id          { Faker::IDNumber.valid }
    giorno           { Faker::Number.number(digits: 2) }
    mese             { Faker::Lorem.word }
    anno             {Faker::Number.number(digits: 4)}
    stagione         {Faker::Lorem.word}
    citta            {Faker::Address.city}
    regione          {Faker::Lorem.word}
    stato            {Faker::Lorem.word}
    parola1          {Faker::Lorem.word}
    parola2          {Faker::Lorem.word}
    parola3          {Faker::Lorem.word}
    somma            {Faker::Number.number(digits: 2)}
    parola4          {Faker::Lorem.word}
    parola5          {Faker::Lorem.word}
    parola6          {Faker::Lorem.word}
    oggetto1         {Faker::Lorem.word}
    oggetto2         {Faker::Lorem.word}
    frase            {Faker::Number.number(digits: 1)}
  end
end 




