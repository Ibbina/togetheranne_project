require 'rails_helper'

require "./app/models/associazione.rb"

RSpec.describe AssociazionesController, type: :controller do

    before(:each) do
        @user = FactoryBot.create(:user)
	@user_d = FactoryBot.create(:user)
        @associazione = FactoryBot.create(:associazione, user_id:@user.id, codicea: @user_d.id )
        @associazione.user_id = @user.id
	
    end
	context "when initialized" do
    subject(:associazione) { Associazione.new }

    it "is a new associazione" do
      expect(associazione).to be_a_new(Associazione)
    end

    it "is not a new string" do
      expect(associazione).not_to be_a_new(String)
    end
  end

  context "when saved" do
    subject(:associazione) { Associazione.create }

    it "is not a new associazione" do
      expect(:associazione).not_to be_a_new(Associazione)
    end

    it "is not a new string" do
      expect(associazione).not_to be_a_new(String)
    end
  end
end





