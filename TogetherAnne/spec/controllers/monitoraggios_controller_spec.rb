require 'rails_helper'

require "./app/models/monitoraggio.rb"

RSpec.describe MonitoraggiosController, type: :controller do

    before(:each) do
        @user = FactoryBot.create(:user)
        @monitoraggio = FactoryBot.create(:monitoraggio, user_id: @user.id)
    end


    
	context "when initialized" do
    subject(:monitoraggio) { Monitoraggio.new }

    it "is a new monitoraggio" do
      expect(monitoraggio).to be_a_new(Monitoraggio)
    end

    it "is not a new string" do
      expect(monitoraggio).not_to be_a_new(String)
    end
  end

  context "when saved" do
    subject(:monitoraggio) { Monitoraggio.create }

    it "is not a new monitoraggio" do
      expect(:monitoraggio).not_to be_a_new(Monitoraggio)
    end

    it "is not a new string" do
      expect(monitoraggio).not_to be_a_new(String)
    end
  end

    
end


