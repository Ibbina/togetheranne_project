json.extract! associazione, :id, :nomea, :cognomea, :codicea, :created_at, :updated_at
json.url associazione_url(associazione, format: :json)
