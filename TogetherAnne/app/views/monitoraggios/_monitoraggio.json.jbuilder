json.extract! monitoraggio, :id, :data, :orientamentosp, :memoria, :calcolo, :richiamo, :linguaggio, :totale, :created_at, :updated_at
json.url monitoraggio_url(monitoraggio, format: :json)
