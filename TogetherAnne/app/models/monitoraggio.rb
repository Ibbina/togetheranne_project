class Monitoraggio < ApplicationRecord
   belongs_to :user, :foreign_key => 'user_id'
   validates :user_id, presence: true
   validates :frase, presence: true
   validates :citta, presence: true
   validates :regione, presence: true
   validates :giorno, presence: true
   validates :mese, presence: true
   validates :anno, presence: true
   def somma_punteggio
	    time= Time.now
	    inverno = ['January', 'February', 'December']
	    autunno = ['September', 'October', 'November']
	    primavera = ['March', 'April', 'May']
	    estate = ['June', 'July', 'August']
	    if inverno.include?(time.strftime("%B"))
	    	stagionee = 'Inverno'
	    elsif autunno.include?(time.strftime("%B"))
	    	stagionee = 'Autunno'
	    elsif primavera.include?(time.strftime("%B"))
	    	stagionee = 'Primavera'
	    elsif estate.include?(time.strftime("%B"))
	    	stagionee = 'Estate'
	    end
	    self.data = time
	    self.orientamentosp = 0
	    self.calcolo=0
	    self.memoria=0
	    self.richiamo=0
	    self.linguaggio=0
	    self.totale=0
	    if self.giorno == time.day
            	self.orientamentosp +=1
	    end
            if self.mese == time.strftime("%B")
		self.orientamentosp +=1
	    end
            if self.anno == time.year
		self.orientamentosp +=1
            end
	    if self.stagione == stagionee
            	self.orientamentosp +=1
	    end
            if self.citta == self.citta
		self.orientamentosp +=1
	    end
	    if self.regione == self.regione
		self.orientamentosp +=1
	    end
            if self.stato == 'Italia'
		self.orientamentosp +=1
	    end
	    if self.parola1 == 'cane'
		self.memoria +=1
	    end
	    if self.parola2 == 'casa'
		self.memoria +=1
	    end
	    if self.parola3 == 'forchetta'
		self.memoria +=1
	    end
	    if self.somma == 32
		self.calcolo = 5
	    end
            if self.parola4 == 'cane'
		self.richiamo +=1
	    end
	    if self.parola5 == 'casa'
		self.richiamo +=1
	    end
	    if self.parola6 == 'forchetta'
            	self.richiamo +=1
	    end
	    if self.oggetto1 == 'palla'
		self.linguaggio +=1
	    end
	    if self.oggetto2 == 'orologio'
  		self.linguaggio +=1
	    end
	    if self.frase == 2
		self.linguaggio +=1
	    end
	    self.totale = self.orientamentosp + self.memoria + self.calcolo + self.richiamo + self.linguaggio
   end
end
