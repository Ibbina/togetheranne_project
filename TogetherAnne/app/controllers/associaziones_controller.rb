class AssociazionesController < ApplicationController


  # GET /associaziones
  # GET /associaziones.json
  def index
    #@associaziones = Associazione.all
  end

  # GET /associaziones/1
  # GET /associaziones/1.json
  def show
	@associazione = User.find(params[:user_id]).associazione
        @user = User.find(params[:user_id])
  end

  # GET /associaziones/new
  def new
    @associazione = Associazione.new
  end

  # GET /associaziones/1/edit
  def edit
  end

  # POST /associaziones
  # POST /associaziones.json
  def create
 	#@user = current_user
	
        	@associazione = current_user.build_associazione(associazione_params)

   # @associazione = Associazione.new(associazione_params)
   # @user = current_user
    #@associazione = @user.build(associazione_params)
		
			if @associazione.save
			    con = @associazione.codicea
			    if (User.exists?(@associazione.codicea) != false && (User.exists?(nome: @associazione.nomea)) == true && (User.exists?(cognome: @associazione.cognomea)) == true )
			    	flash[:success] = "L'associazione è riuscita con successo"
			    	redirect_to root_path
			    else
				@associazione.destroy
				flash[:danger] = "ATTENZIONE! i campi inseriti per l'associazione sono errati"
				redirect_to root_path
				return
			    end
			else
			    flash[:danger] = "ATTENZIONE! Non è stato possibile portare a termine l'associazione"
			    redirect_to root_path
			    return
			end

		
		

    #respond_to do |format|
     # if @associazione.save
     #   format.html { redirect_to @associazione, notice: 'Associazione was successfully created.' }
    #    format.json { render :show, status: :created, location: @associazione }
   #   else
    #    format.html { render :new }
    #    format.json { render json: @associazione.errors, status: :unprocessable_entity }
   #   end
  #  end
  end

  # PATCH/PUT /associaziones/1
  # PATCH/PUT /associaziones/1.json
  def update
    respond_to do |format|
      if @associazione.update(associazione_params)
        format.html { redirect_to @associazione, notice: 'Associazione was successfully updated.' }
        format.json { render :show, status: :ok, location: @associazione }
      else
        format.html { render :edit }
        format.json { render json: @associazione.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /associaziones/1
  # DELETE /associaziones/1.json
  def destroy
    current_user.associazione.destroy
    flash[:success] = "L'associazione è stata cancellata con successo"
    redirect_to root_path
      
    
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_associazione
      @associazione = Associazione.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def associazione_params
      params.require(:associazione).permit(:nomea, :cognomea, :codicea)
    end
end
