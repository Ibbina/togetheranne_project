class UsersController < ApplicationController
before_action :current_user, only: [:index, :show, :edit, :update, :destroy]


  def index
    @user = current_user
    
  end


  
  def show
    if (params[:id]).nil? then
      redirect_to root_path
      return
    end
    @user = User.find(params[:id])
  end


  def new
    @user = User.new
  end


  def edit
    @user = User.find(params[:id])
  end


  def create
    @user = User.new(user_params)
	
    if @user.save
	flash[:success] = "Utente inserito"
	@users = User.all
	@hash = Gmaps4rails.build_markers(@users) do |user, marker|
  	marker.lat user.latitude
  	marker.lng user.longitude
	end
        redirect_to user_path(current_user)
       
      else
        flash[:danger] = "Utente non inserito"
        render :new
    end
	
  end


  def update
    @user = current_user
      if @user.update_attributes(params.require(:user).permit(:nome, :cognome, :email, :indirizzo, :luogonascita, :date, :demenza))
        redirect_to user_path(@user)
      else
        render :edit
      end
  end

  def destroy
    @user = User.find(params[:id]).destroy
    redirect_to root_path
    return
  end



  private
    # Utilizza i callback per condividere l'impostazione o i vincoli comuni tra le azioni.
    def set_user
      @user = User.find(params[:id])
    end

    # Consenti solo un elenco di parametri attendibili.
    def user_params
      params.require(:user).permit(:nome, :cognome, :email, :indirizzo, :luogonascita, :date, :demenza, :password)
    end


    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless @user == current_user
    end

  
	

end
