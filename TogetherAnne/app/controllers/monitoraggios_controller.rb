class MonitoraggiosController < ApplicationController

  # GET /monitoraggios
  # GET /monitoraggios.json
 # def index
    #@monitoraggios = Monitoraggio.all
 # end

  # GET /monitoraggios/1
  # GET /monitoraggios/1.json
  def show
	@monitorragios = User.find(params[:user_id])
        @user = User.find(params[:user_id])
  end

  # GET /monitoraggios/new
  def new
    @monitoraggio = Monitoraggio.new
  end

  # GET /monitoraggios/1/edit
  def edit
  end

  # POST /monitoraggios
  # POST /monitoraggios.json
  def create
    #@monitoraggio = Monitoraggio.new(monitoraggio_params)
   
    #@user = current_user
    @monitoraggio = current_user.monitoraggios.build(monitoraggio_params)
        if @monitoraggio.save
            flash[:success] = "I risultati sono stati salvati, per visualizzarli vai su Monitora i tuoi risultati"
            redirect_to root_path
		
	else
            flash[:danger] = "ATTENZIONE! Non è stato possibile crearlo"
            redirect_to root_path
            return
        end

   # respond_to do |format|
     # if @monitoraggio.save
      #  format.html { redirect_to @monitoraggio, notice: 'Monitoraggio was successfully created.' }
       # format.json { render :show, status: :created, location: @monitoraggio }
     # else
     #   format.html { render :new }
      #  format.json { render json: @monitoraggio.errors, status: :unprocessable_entity }
     # end
   # end
  end

  # PATCH/PUT /monitoraggios/1
  # PATCH/PUT /monitoraggios/1.json
  def update
    respond_to do |format|
      if @monitoraggio.update(monitoraggio_params)
        format.html { redirect_to @monitoraggio, notice: 'Monitoraggio was successfully updated.' }
        format.json { render :show, status: :ok, location: @monitoraggio }
      else
        format.html { render :edit }
        format.json { render json: @monitoraggio.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /monitoraggios/1
  # DELETE /monitoraggios/1.json
  def destroy
    @monitoraggio.destroy
    respond_to do |format|
      format.html { redirect_to monitoraggios_url, notice: 'Monitoraggio was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_monitoraggio
      @monitoraggio = Monitoraggio.find(params[:user_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def monitoraggio_params
      params.require(:monitoraggio).permit(:data, :orientamentosp, :memoria, :calcolo, :richiamo, :linguaggio, :totale, :giorno, :mese, :anno, :stagione, :citta, :regione, :stato, :parola1, :parola2, :parola3, :somma, :parola4, :parola5, :parola6, :oggetto1, :oggetto2, :frase, :punteggio)   
    end  
end
