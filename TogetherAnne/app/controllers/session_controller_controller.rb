class SessionControllerController < ApplicationController
   def new
   end

    def create
        user = User.find_by(email: params[:session][:email].downcase)
        if user && user.authenticate(params[:session][:password])
            log_in user
            redirect_to root_url
        else
            flash.now[:danger] = 'Email o password errati'
            render 'new'
        end
    end

    def destroy
        log_out if logged_in? # fa il logout solo se l'utente è entrato
        redirect_to root_url
    end

end
