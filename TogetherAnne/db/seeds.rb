# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
I18n.reload!


User.create!(nome:  "Marco",
             cognome: "Sossi",
             email: "marcosossi@gmail.com",
             password:              "Marcosossi",
             password_confirmation: "Marcosossi",
             luogonascita: "Roma",
             indirizzo: "Via Milano",
             demenza: false,
	     codice: "")

