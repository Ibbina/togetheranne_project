class CreateMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    create_table :monitoraggios do |t|
      t.datetime :data
      t.integer :orientamentosp
      t.integer :memoria
      t.integer :calcolo
      t.integer :richiamo
      t.integer :linguaggio
      t.integer :totale

      t.timestamps
    end
  end
end
