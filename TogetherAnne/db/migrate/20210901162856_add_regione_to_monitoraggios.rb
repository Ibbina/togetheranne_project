class AddRegioneToMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    add_column :monitoraggios, :regione, :string
  end
end
