class CreateAssociaziones < ActiveRecord::Migration[5.1]
  def change
    create_table :associaziones do |t|
      t.string :nomea
      t.string :cognomea
      t.integer :codicea

      t.timestamps
    end
  end
end
