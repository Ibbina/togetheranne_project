class AddGiornoToMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    add_column :monitoraggios, :giorno, :integer
  end
end
