class AddStagioneToMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    add_column :monitoraggios, :stagione, :string
  end
end
