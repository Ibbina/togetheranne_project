class AddSommaToMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    add_column :monitoraggios, :somma, :integer
  end
end
