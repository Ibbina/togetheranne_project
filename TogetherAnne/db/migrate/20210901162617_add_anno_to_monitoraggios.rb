class AddAnnoToMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    add_column :monitoraggios, :anno, :integer
  end
end
