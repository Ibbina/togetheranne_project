class AddUserIdToMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    add_column :monitoraggios, :user_id, :integer
  end
end
