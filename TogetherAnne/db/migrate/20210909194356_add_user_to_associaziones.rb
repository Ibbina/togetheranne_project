class AddUserToAssociaziones < ActiveRecord::Migration[5.1]
  def change
    add_reference :associaziones, :user, foreign_key: true
  end
end
