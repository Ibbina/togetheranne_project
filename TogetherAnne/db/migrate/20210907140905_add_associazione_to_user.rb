class AddAssociazioneToUser < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :associazione, :integer
  end
end
