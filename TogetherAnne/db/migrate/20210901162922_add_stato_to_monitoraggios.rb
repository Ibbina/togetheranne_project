class AddStatoToMonitoraggios < ActiveRecord::Migration[5.1]
  def change
    add_column :monitoraggios, :stato, :string
  end
end
