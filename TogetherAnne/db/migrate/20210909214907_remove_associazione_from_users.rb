class RemoveAssociazioneFromUsers < ActiveRecord::Migration[5.1]
  def change
    remove_column :users, :associazione, :integer
  end
end
