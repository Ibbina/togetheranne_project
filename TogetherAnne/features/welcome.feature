Feature: Home
	As a Guest
	I want to see the home page of my app
  	In order to Registrati o Accedi
  	

	Scenario: View home page
  		Given I am on the home page
  		Then I should see 'Benvenuto in TogethereAnne Registrati Accedi Non vuoi registrarti? Visita la parte dei suggerimenti visibile a tuttiVai ai suggerimenti About us'
                Then I should see 'Vai ai suggerimenti'
		
        
        Scenario: Guest can login
		Given I am on the home page
		And I click Accedi
		Then I should be on the login page

        Scenario: Guest can sign up
		Given I am on the home page
		And I click Registrati
		Then I should be on the sign up page
	
	Scenario: Guest can see suggerimenti
		Given I am on the home page
		And I click Vai ai suggerimenti
		Then I should be on the suggerimenti page

