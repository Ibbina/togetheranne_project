Feature: Guest can login
    	As a guest
    	I want to login with email
    	So that I can join it

        Scenario: Login success
            Given I am on the login page
            And I insert my credentials
            When I click button Accedi
            Then I should be on the home page

        Scenario: Login failed (password too short)
            Given I am on the login page
            When I fill in "user[email]" with "exam@email.com"
            And I fill in "user[password]" with "po"
            And I click button Accedi
            Then I should be on the sign in page
            And I should see 'Invalid Email or password'
 	
	Scenario: Login failed (email invalid)
            Given I am on the login page
            When I fill in "user[email]" with "exam"
            And I fill in "user[password]" with "popopo"
            And I click button Accedi
            Then I should be on the sign in page
            And I should see 'Invalid Email or password'

