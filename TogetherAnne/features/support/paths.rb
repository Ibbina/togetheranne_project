module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  #   When /^I go to (.+)$/ do |page_name|
  #
  # step definition in web_steps.rb
  #

  def path_to(page_name)
    case page_name

    when /^the home\s?page$/
      '/'

     when /^the profile page$/
	user_path(@user.id)

    when /^the sign in page$/
	'/users/sign_in'

    when /^the monitoraggio page$/
      '/users/1/monitoraggios/new'

    when /^the suggerimenti page$/
      '/static_pages/suggerimenti'
   
    when /^the associazione page$/
      '/users/1/associazione/new'

    when /^the sign up page$/
      '/users/sign_up'

    when /^my profile page$/
      user_path(@user.id)

    when /^the login page$/
      '/users/sign_in'

    when /^the logout page$/
	'/root'

    when /^the home page$/
	'/root'
    else
      begin
        page_name =~ /^the (.*) page$/
        path_components = $1.split(/\s+/)
        self.send(path_components.push('path').join('_').to_sym)
      rescue NoMethodError, ArgumentError
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
end

World(NavigationHelpers)



