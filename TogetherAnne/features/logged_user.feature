Feature: User can see his home
        As a User
        I want to see my home


	Scenario: See my home
	   Given I am a registered user
	   And I am on the home page
	   And I should see 'Vai ai suggerimenti'

	Scenario: See my home
	   Given I am a registered user
	   And I am on the home page
	   And I should see 'Home'
  
	Scenario: See my home
	   Given I am a registered user
	   And I am on the home page
	   And I should see 'Log out'

	Scenario: See my home
	   Given I am a registered user
	   And I am on the home page
	   And I should see 'Profilo'
