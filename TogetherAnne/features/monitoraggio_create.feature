Feature: As a user 
         I want to create my monitoraggio
        
	Scenario: Create my monitoraggio
	   Given I am logged in
           And I am on the home page
           When I click Inizia il test
           Then I should be on the monitoraggio page
           And I fill in "monitoraggio[giorno]" with "13" 
           And I fill in "monitoraggio[mese]" with "Testtest"
	   And I fill in "monitoraggio[anno]" with "1000"
	   And I fill in "monitoraggio[stagione]" with "Autunno"
           And I fill in "monitoraggio[citta]" with "Roma"
	   And I fill in "monitoraggio[regione]" with "Lazio"
	   And I fill in "monitoraggio[stato]" with "Italia"
	   And I fill in "monitoraggio[parola1]" with "ppp"
           And I fill in "monitoraggio[parola2]" with "ppp"
           And I fill in "monitoraggio[parola3]" with "ppp"
	   And I fill in "monitoraggio[somma]" with "10"
	   And I fill in "monitoraggio[parola4]" with "ppp"
	   And I fill in "monitoraggio[parola5]" with "ppp"
	   And I fill in "monitoraggio[parola6]" with "ppp"
 	   And I fill in "monitoraggio[oggetto1]" with "ppp"
	   And I fill in "monitoraggio[oggetto2]" with "ppp"
	   And I fill in "monitoraggio[frase]" with "1"
           And I click button Invia
	   Then I should be on the home page 
 	
 	Scenario: Create my monitoraggio (without compile frase)
	   Given I am logged in
           And I am on the home page
           When I click Inizia il test
           Then I should be on the monitoraggio page
           And I fill in "monitoraggio[giorno]" with "13" 
           And I fill in "monitoraggio[mese]" with "Testtest"
	   And I fill in "monitoraggio[anno]" with "1000"
	   And I fill in "monitoraggio[stagione]" with "Autunno"
           And I fill in "monitoraggio[citta]" with "Roma"
	   And I fill in "monitoraggio[regione]" with "Lazio"
	   And I fill in "monitoraggio[stato]" with "Italia"
	   And I fill in "monitoraggio[parola1]" with "ppp"
           And I fill in "monitoraggio[parola2]" with "ppp"
           And I fill in "monitoraggio[parola3]" with "ppp"
	   And I fill in "monitoraggio[somma]" with "10"
	   And I fill in "monitoraggio[parola4]" with "ppp"
	   And I fill in "monitoraggio[parola5]" with "ppp"
	   And I fill in "monitoraggio[parola6]" with "ppp"
 	   And I fill in "monitoraggio[oggetto1]" with "ppp"
	   And I fill in "monitoraggio[oggetto2]" with "ppp"
           And I click button Invia
	   Then I should be on the home page 
           And I should see 'Notification: ATTENZIONE! Non è stato possibile crearlo'

	Scenario: Create my monitoraggio (without compile citta)
	   Given I am logged in
           And I am on the home page
           When I click Inizia il test
           Then I should be on the monitoraggio page
           And I fill in "monitoraggio[giorno]" with "13" 
           And I fill in "monitoraggio[mese]" with "Testtest"
	   And I fill in "monitoraggio[anno]" with "1000"
	   And I fill in "monitoraggio[stagione]" with "Autunno"
	   And I fill in "monitoraggio[regione]" with "Lazio"
	   And I fill in "monitoraggio[stato]" with "Italia"
	   And I fill in "monitoraggio[parola1]" with "ppp"
           And I fill in "monitoraggio[parola2]" with "ppp"
           And I fill in "monitoraggio[parola3]" with "ppp"
	   And I fill in "monitoraggio[somma]" with "10"
	   And I fill in "monitoraggio[parola4]" with "ppp"
	   And I fill in "monitoraggio[parola5]" with "ppp"
	   And I fill in "monitoraggio[parola6]" with "ppp"
 	   And I fill in "monitoraggio[oggetto1]" with "ppp"
	   And I fill in "monitoraggio[oggetto2]" with "ppp"
           And I fill in "monitoraggio[frase]" with "1"
           And I click button Invia
	   Then I should be on the home page 
           And I should see 'Notification: ATTENZIONE! Non è stato possibile crearlo'

	Scenario: Create my monitoraggio (without compile regione)
	   Given I am logged in
           And I am on the home page
           When I click Inizia il test
           Then I should be on the monitoraggio page
           And I fill in "monitoraggio[giorno]" with "13" 
           And I fill in "monitoraggio[mese]" with "Testtest"
	   And I fill in "monitoraggio[anno]" with "1000"
	   And I fill in "monitoraggio[stagione]" with "Autunno"
	   And I fill in "monitoraggio[citta]" with "Roma"
	   And I fill in "monitoraggio[stato]" with "Italia"
	   And I fill in "monitoraggio[parola1]" with "ppp"
           And I fill in "monitoraggio[parola2]" with "ppp"
           And I fill in "monitoraggio[parola3]" with "ppp"
	   And I fill in "monitoraggio[somma]" with "10"
	   And I fill in "monitoraggio[parola4]" with "ppp"
	   And I fill in "monitoraggio[parola5]" with "ppp"
	   And I fill in "monitoraggio[parola6]" with "ppp"
 	   And I fill in "monitoraggio[oggetto1]" with "ppp"
	   And I fill in "monitoraggio[oggetto2]" with "ppp"
           And I fill in "monitoraggio[frase]" with "1"
           And I click button Invia
	   Then I should be on the home page 
           And I should see 'Notification: ATTENZIONE! Non è stato possibile crearlo'

	Scenario: Create my monitoraggio (without compile giorno)
	   Given I am logged in
           And I am on the home page
           When I click Inizia il test
           Then I should be on the monitoraggio page
           And I fill in "monitoraggio[mese]" with "Testtest"
	   And I fill in "monitoraggio[anno]" with "1000"
	   And I fill in "monitoraggio[stagione]" with "Autunno"
	   And I fill in "monitoraggio[citta]" with "Roma"
	   And I fill in "monitoraggio[regione]" with "Lazio"
	   And I fill in "monitoraggio[stato]" with "Italia"
	   And I fill in "monitoraggio[parola1]" with "ppp"
           And I fill in "monitoraggio[parola2]" with "ppp"
           And I fill in "monitoraggio[parola3]" with "ppp"
	   And I fill in "monitoraggio[somma]" with "10"
	   And I fill in "monitoraggio[parola4]" with "ppp"
	   And I fill in "monitoraggio[parola5]" with "ppp"
	   And I fill in "monitoraggio[parola6]" with "ppp"
 	   And I fill in "monitoraggio[oggetto1]" with "ppp"
	   And I fill in "monitoraggio[oggetto2]" with "ppp"
           And I fill in "monitoraggio[frase]" with "1"
           And I click button Invia
	   Then I should be on the home page 
           And I should see 'Notification: ATTENZIONE! Non è stato possibile crearlo'

	Scenario: Create my monitoraggio (without compile mese)
	   Given I am logged in
           And I am on the home page
           When I click Inizia il test
           Then I should be on the monitoraggio page
           And I fill in "monitoraggio[giorno]" with "13"
	   And I fill in "monitoraggio[anno]" with "1000"
	   And I fill in "monitoraggio[stagione]" with "Autunno"
	   And I fill in "monitoraggio[citta]" with "Roma"
	   And I fill in "monitoraggio[regione]" with "Lazio"
	   And I fill in "monitoraggio[stato]" with "Italia"
	   And I fill in "monitoraggio[parola1]" with "ppp"
           And I fill in "monitoraggio[parola2]" with "ppp"
           And I fill in "monitoraggio[parola3]" with "ppp"
	   And I fill in "monitoraggio[somma]" with "10"
	   And I fill in "monitoraggio[parola4]" with "ppp"
	   And I fill in "monitoraggio[parola5]" with "ppp"
	   And I fill in "monitoraggio[parola6]" with "ppp"
 	   And I fill in "monitoraggio[oggetto1]" with "ppp"
	   And I fill in "monitoraggio[oggetto2]" with "ppp"
           And I fill in "monitoraggio[frase]" with "1"
           And I click button Invia
	   Then I should be on the home page 
           And I should see 'Notification: ATTENZIONE! Non è stato possibile crearlo'

	Scenario: Create my monitoraggio (without compile anno)
	   Given I am logged in
           And I am on the home page
           When I click Inizia il test
           Then I should be on the monitoraggio page
	   And I fill in "monitoraggio[giorno]" with "13"
           And I fill in "monitoraggio[mese]" with "Testtest"
	   And I fill in "monitoraggio[stagione]" with "Autunno"
	   And I fill in "monitoraggio[citta]" with "Roma"
	   And I fill in "monitoraggio[regione]" with "Lazio"
	   And I fill in "monitoraggio[stato]" with "Italia"
	   And I fill in "monitoraggio[parola1]" with "ppp"
           And I fill in "monitoraggio[parola2]" with "ppp"
           And I fill in "monitoraggio[parola3]" with "ppp"
	   And I fill in "monitoraggio[somma]" with "10"
	   And I fill in "monitoraggio[parola4]" with "ppp"
	   And I fill in "monitoraggio[parola5]" with "ppp"
	   And I fill in "monitoraggio[parola6]" with "ppp"
 	   And I fill in "monitoraggio[oggetto1]" with "ppp"
	   And I fill in "monitoraggio[oggetto2]" with "ppp"
           And I fill in "monitoraggio[frase]" with "1"
           And I click button Invia
	   Then I should be on the home page 
           And I should see 'Notification: ATTENZIONE! Non è stato possibile crearlo'
