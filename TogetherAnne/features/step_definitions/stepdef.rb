require 'uri'
Given /^I am a registered user$/ do
  @user = FactoryBot.create(:user)
  visit "users/sign_in"
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  click_button "Accedi"
end

Given /^I am logged in without demenza$/ do
  visit "users/sign_in"
  @user = FactoryBot.create(:user)
  @associazione = FactoryBot.create(:associazione, @user.id)
  @userd = FactoryBot.create(:user)
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  @user.demenza=false
  fill_in "user_email", :with => @userd.email
  fill_in "user_password", :with => @userd.password
  fill_in "associazione_codicea", :with => @userd.id
  click_button "Accedi"
end

Given /^I am logged in$/ do
  visit "users/sign_in"
  @user = FactoryBot.create(:user)
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
  click_button "Accedi"
end


When /^I click (.*)/ do |element|
   click_on(element).click
end

When /^(?:|I )go to (.+)$/ do |page_name|
  visit path_to(page_name)
end

When /^I click button (.*)/ do |elem|
	 click_button(elem)
end

When /^(?:|I )fill in "([^"]*)" with "([^"]*)"$/ do |field, value|
  @user = FactoryBot.create(:user)
  fill_in(field, :with => value)
end

When /^I attach an "([^\"]*)" image to "([^\"]*)"$/ do |size, field|
  attach_file(field, image_path(size))
end

Given /^(?:|I )am on (.+)$/ do |page_name|
  visit path_to(page_name)
end

When /^I check (.*)/ do |element|
	check(element).set(true)
end

Then /^(?:|I )should be on (.+)$/ do |page_name|

  current_path = URI.parse(current_url).path
  if current_path.respond_to? :should
    expect(current_path).to be == path_to(page_name)
  else
    assert_equal path_to(page_name), current_path
  end
end


Then /^(?:|I )should see '([^"]*)'$/ do |text|
  if page.respond_to? :should
    expect(page).to have_content(text)
  else
    assert page.has_content?(text)
  end
end


Then /^(?:|I )should see \/([^\/]*)\/$/ do |regexp|
  regexp = Regexp.new(regexp)

  if page.respond_to? :should
    expect(page).to have_xpath('//*', :text => regexp)
  else
    assert page.has_xpath?('//*', :text => regexp)
  end
end


Then /^(?:|I )should not see '([^"]*)'$/ do |text|
  if page.respond_to? :should
    expect(page).to have_no_content(text)
  else
    assert page.has_no_content?(text)
  end
end

When /^(?:|I )press '([^"]*)'$/ do |button|
  click_button(button)
end


And /^I have an account$/ do
  @user = FactoryBot.create(:user)
end


And /^I insert my credentials$/ do
     @user = FactoryBot.create(:user)
  fill_in "user_email", :with => @user.email
  fill_in "user_password", :with => @user.password
end

Then /^(?:|I )should see the text'([^"]*)'$/ do |text|
  if page.respond_to? :should
    expect(page).to have_content(text)
  else
    assert page.has_content?(text)
  end
end












