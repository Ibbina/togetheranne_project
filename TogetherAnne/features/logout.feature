Feature: User can logout

Scenario: Logout success
	Given I am on the home page
	And I am logged in
	When I click Log out
	Then I should be on the home page
	And I should see 'Registrati'
	
