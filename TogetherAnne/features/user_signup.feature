Feature: Guest can create a new account
	As a guest
	I want to sign up
	So I can be a user


Scenario: Create a new user account
	Given I am on the sign up page
	When I fill in "user[nome]" with "Test"
	And I fill in "user[cognome]" with "Bho"
	And I fill in "user[email]" with "bho@gmail.com"
        And I fill in "user[indirizzo]" with "via milano"
        And I fill in "user[citta]" with "Roma"
	And I fill in "user[regione]" with "Lazio"
	And I fill in "user[luogonascita]" with "Roma"
	And I fill in "user[date]" with "12/01/1990"
	And I fill in "user[password]" with "popopo"
	And I fill in "user[password_confirmation]" with "popopo"
	And I click Crea il mio account
	Then I should be on the home page
	





